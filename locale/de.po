#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "field:res.role,create_date:"
msgid "Create Date"
msgstr "Erstellungsdatum"

msgctxt "field:res.role,create_uid:"
msgid "Create User"
msgstr "Erstellt durch"

msgctxt "field:res.role,groups:"
msgid "Groups"
msgstr "Gruppen"

msgctxt "field:res.role,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:res.role,name:"
msgid "Name"
msgstr "Name"

msgctxt "field:res.role,rec_name:"
msgid "Record Name"
msgstr "Bezeichnung des Datensatzes"

msgctxt "field:res.role,write_date:"
msgid "Write Date"
msgstr "Zuletzt geändert"

msgctxt "field:res.role,write_uid:"
msgid "Write User"
msgstr "Letzte Änderung durch"

msgctxt "field:res.role-res.group,create_date:"
msgid "Create Date"
msgstr "Erstellungsdatum"

msgctxt "field:res.role-res.group,create_uid:"
msgid "Create User"
msgstr "Erstellt durch"

msgctxt "field:res.role-res.group,group:"
msgid "Group"
msgstr "Gruppe"

msgctxt "field:res.role-res.group,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:res.role-res.group,rec_name:"
msgid "Record Name"
msgstr "Bezeichnung des Datensatzes"

msgctxt "field:res.role-res.group,role:"
msgid "Role"
msgstr "Rolle"

msgctxt "field:res.role-res.group,write_date:"
msgid "Write Date"
msgstr "Zuletzt geändert"

msgctxt "field:res.role-res.group,write_uid:"
msgid "Write User"
msgstr "Letzte Änderung durch"

msgctxt "field:res.user,roles:"
msgid "Roles"
msgstr "Rollen"

msgctxt "field:res.user.role,create_date:"
msgid "Create Date"
msgstr "Erstellungsdatum"

msgctxt "field:res.user.role,create_uid:"
msgid "Create User"
msgstr "Erstellt durch"

msgctxt "field:res.user.role,from_date:"
msgid "From Date"
msgstr "Von Datum"

msgctxt "field:res.user.role,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:res.user.role,rec_name:"
msgid "Record Name"
msgstr "Bezeichnung des Datensatzes"

msgctxt "field:res.user.role,role:"
msgid "Role"
msgstr "Rolle"

msgctxt "field:res.user.role,to_date:"
msgid "To Date"
msgstr "Bis Datum"

msgctxt "field:res.user.role,user:"
msgid "User"
msgstr "Benutzer"

msgctxt "field:res.user.role,write_date:"
msgid "Write Date"
msgstr "Zuletzt geändert"

msgctxt "field:res.user.role,write_uid:"
msgid "Write User"
msgstr "Letzte Änderung durch"

msgctxt "model:ir.action,name:act_role_form"
msgid "Roles"
msgstr "Rollen"

msgctxt "model:ir.ui.menu,name:menu_role_form"
msgid "Roles"
msgstr "Rollen"

msgctxt "model:res.role,name:"
msgid "Role"
msgstr "Rolle"

msgctxt "model:res.role-res.group,name:"
msgid "Role - Group"
msgstr "Rolle - Gruppe"

msgctxt "model:res.user.role,name:"
msgid "User Role"
msgstr "Benutzerrolle"

msgctxt "selection:ir.cron,method:"
msgid "Synchronize Users Roles"
msgstr "Benutzerrollen synchronisieren"
