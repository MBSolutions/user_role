#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "field:res.role,create_date:"
msgid "Create Date"
msgstr ""

msgctxt "field:res.role,create_uid:"
msgid "Create User"
msgstr ""

msgctxt "field:res.role,groups:"
msgid "Groups"
msgstr ""

msgctxt "field:res.role,id:"
msgid "ID"
msgstr ""

msgctxt "field:res.role,name:"
msgid "Name"
msgstr ""

msgctxt "field:res.role,rec_name:"
msgid "Record Name"
msgstr ""

msgctxt "field:res.role,write_date:"
msgid "Write Date"
msgstr ""

msgctxt "field:res.role,write_uid:"
msgid "Write User"
msgstr ""

msgctxt "field:res.role-res.group,create_date:"
msgid "Create Date"
msgstr ""

msgctxt "field:res.role-res.group,create_uid:"
msgid "Create User"
msgstr ""

msgctxt "field:res.role-res.group,group:"
msgid "Group"
msgstr ""

msgctxt "field:res.role-res.group,id:"
msgid "ID"
msgstr ""

msgctxt "field:res.role-res.group,rec_name:"
msgid "Record Name"
msgstr ""

msgctxt "field:res.role-res.group,role:"
msgid "Role"
msgstr ""

msgctxt "field:res.role-res.group,write_date:"
msgid "Write Date"
msgstr ""

msgctxt "field:res.role-res.group,write_uid:"
msgid "Write User"
msgstr ""

msgctxt "field:res.user,roles:"
msgid "Roles"
msgstr ""

msgctxt "field:res.user.role,create_date:"
msgid "Create Date"
msgstr ""

msgctxt "field:res.user.role,create_uid:"
msgid "Create User"
msgstr ""

msgctxt "field:res.user.role,from_date:"
msgid "From Date"
msgstr ""

msgctxt "field:res.user.role,id:"
msgid "ID"
msgstr ""

msgctxt "field:res.user.role,rec_name:"
msgid "Record Name"
msgstr ""

msgctxt "field:res.user.role,role:"
msgid "Role"
msgstr ""

msgctxt "field:res.user.role,to_date:"
msgid "To Date"
msgstr ""

msgctxt "field:res.user.role,user:"
msgid "User"
msgstr ""

msgctxt "field:res.user.role,write_date:"
msgid "Write Date"
msgstr ""

msgctxt "field:res.user.role,write_uid:"
msgid "Write User"
msgstr ""

msgctxt "model:ir.action,name:act_role_form"
msgid "Roles"
msgstr ""

msgctxt "model:ir.ui.menu,name:menu_role_form"
msgid "Roles"
msgstr ""

msgctxt "model:res.role,name:"
msgid "Role"
msgstr ""

msgctxt "model:res.role-res.group,name:"
msgid "Role - Group"
msgstr ""

msgctxt "model:res.user.role,name:"
msgid "User Role"
msgstr ""

msgctxt "selection:ir.cron,method:"
msgid "Synchronize Users Roles"
msgstr ""
